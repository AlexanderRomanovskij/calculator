package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean enter = true;
        while (enter == true){
            try {
                Scanner input = new Scanner(System.in);

                Calculator calculator = new Calculator();

                double answer = 0;
                double inputA, inputB;
                char operator;
                boolean done = false;
                String s = "";
                while (done == false) {
                    System.out.print("Please enter your sum (InputA operator InputB): ");

                    inputA = input.nextDouble();
                    operator = input.next().charAt(0);
                    inputB = input.nextDouble();

                    switch (operator) {
                        case '+':
                            answer = calculator.add(inputA, inputB);
                            break;
                        case '-':
                            answer = calculator.subtract(inputA, inputB);
                            break;
                        case '*':
                            answer = calculator.multiply(inputA, inputB);
                            break;
                        case '/':
                            answer = calculator.divide(inputA, inputB);
                            break;
                        case '^':
                            answer = calculator.power(inputA, inputB);
                            break;
                    }

                    System.out.println(answer);
                    System.out.println("Continue? (Y or N): ");

                    s = String.valueOf(input.next().charAt(0));
                    if (s.equals("Y")) {
                        done = false;
                    } else done = true;
                }
                input.close();
                enter=false;
            } catch (Exception ex) {
                System.out.println("incorrectly data");
                enter=true;
            }
        }
    }
}
